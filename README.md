# Python Web Client 
Web client made as a school project. Connects to a test server with given address and port and uses TCP to receive id token and the server's UDP port plus to negotiate encryption and multipart messaging. Subsequent communication happens over UDP.

1. Client sends HELLO message to the server using TCP
 * Encryption keys are sent after
2. Server will respond with its encryption keys
3. Client initiates UDP messaging with a "Hello from 'CID'" message, where CID = client's identification token from the server's hello msg
4. Server responds with a list of random words, e.g. tree car mouse
5. Client responds with the words in reverse order, e.g. mouse car tree
6. This exchange happens x times
7. Finally, server responds with "You replied to x messages with x features. Bye."

### Encryption ###
* 20 keys exchanged in the TCP handshake
* Each message is encrypted with a different key, keys only used once (one-time pad encryption)
* Keys are 64 byte strings of hexadecimal chars
* Enc and decr implemented by XORing the numeric value of each character in the message with the numeric value of the corresponding character in the key
* First message "Hello from 'CID'" is encrypted, last message from server is not (EOM bit set)

### Multipart messaging ###
* Messages can be longer than 64 characters -> divided into multiple packets
* Able to read and send multipart messages