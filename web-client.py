import random
import sys
import socket
import struct

BUFFER_SIZE = 4096
PACKET_FORMAT = '!8s??HH64s'

def reverse_words_in_string(str_to_reverse):
    """
    Takes a string which contains words separated by a space and reverses their order.

    @return: The string with the order of the words reversed.
    """
    # Remove trailing zeros
    str_to_reverse = str_to_reverse.split('\0', 1)[0]

    # Split string into an array of words and reverse it
    str_arr = str_to_reverse.split(' ')
    str_arr.reverse()

    # Join the words back together as a string
    reversed_str = ' '.join(str_arr)

    return reversed_str

def generate_key():
    """
    Generates a random 64 byte string of hexadecimal characters.
    
    @return: The generated string.
    """
    random_value = random.randrange(10**80)
    hexadecimal_str = "%064x" % random_value
    # Limit to 64.
    hexadecimal_str = hexadecimal_str[:64]
    return hexadecimal_str

def encrypt_decrypt_message(message, key, msg_length=64):
    """
    Encrypts or decrypts the message with a key.

    @return: The encrypted or decrypted message.
    """
    processed_message = ""
    
    try:
        for x in range(0, msg_length):
            char_proc = chr(ord(message[x]) ^ ord(key[x]))
            processed_message = processed_message + char_proc
    except IndexError as e:
        print "\nOBS! IndexError: {}\nProcessed message until error: {}\nMsg length: {}\nKey length: {}\n".format(e, processed_message, msg_length, len(key))
    return processed_message

def pieces(message, length=64):
    """
    Parses a string of messages of specified length into an array.
    
    @return: Array of messages.
    """
    pieces = []
    messageToAppend = ""
    for i in range(0, len(message)):
        if len(messageToAppend) == 64:
            pieces.append(messageToAppend)
            messageToAppend = ""
        messageToAppend = messageToAppend + message[i]
    if len(messageToAppend) > 0:
        pieces.append(messageToAppend)
    return pieces

def send_and_receive_tcp(address, port, message):
    # Open TCP socket
    data = ""
    s = socket.socket()
    print "TCP socket opened."
    
    try:
        s.connect((address, port))
        print "TCP socket: Connected to {}, port {}.".format(address, port)

        if "ENC" in message:
            print "Encryption enabled."
        if "MUL" in message:
            print "Multipart messages enabled."

        # Generate and send 20 client encryption keys with the first message
        sent_enc_keys = ""
        for x in range(0, 20):
            key = generate_key()    
            sent_enc_keys = sent_enc_keys + key + "\n"
            if x == 19:
                sent_enc_keys = sent_enc_keys + ".\n"
                s.sendall(message + "\n" + sent_enc_keys)
        
        # Receive reply
        print "\nReceiving data on TCP socket.."
        data = s.recv(BUFFER_SIZE)
        print "Received message: \n{}".format(data)
    except socket.error as e:
        sys.exit("Error on TCP socket: {}".format(e))
    finally:
        s.close()
        print "TCP socket closed.\n"

    # Extract UDP port from received reply
    data_splitted = data.split(' ')
    data_port_and_keys_as_arr = data_splitted[2].splitlines()
    port_udp = int(data_port_and_keys_as_arr[0])
    print "Received UDP port: {}".format(port_udp)
    
    # Extract server's encryption keys from received reply into an array
    recv_enc_keys = data_port_and_keys_as_arr
    recv_enc_keys.pop(0)
    
    # Client generated encryption keys into an array 
    sent_enc_keys = sent_enc_keys.splitlines()
    
    # Extract CID from received reply
    cid = data_splitted[1]
    
    send_and_receive_udp(address, port_udp, cid, sent_enc_keys, recv_enc_keys)
    return

def send_and_receive_udp(address, port, client_id, sent_enc_keys, recv_enc_keys):
    # Keep track of used keys
    sent_enc_key_index = 0
    recv_enc_key_index = 0
    
    # Open UDP socket
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.settimeout(10)
    print "\nUDP socket opened."
    s.connect((address, port))
    print "UDP socket: Connected to {}, port {}.".format(address, port)

    # Pack UDP message
    out_eom = False
    out_ack = True
    out_data_remaining = 0
    message = "Hello from {}".format(client_id)
    out_content_len = len(message)
    out_content = encrypt_decrypt_message(message, sent_enc_keys[sent_enc_key_index], out_content_len)
    sent_enc_key_index = sent_enc_key_index + 1
    data = struct.pack(PACKET_FORMAT, client_id, out_ack, out_eom, out_data_remaining, out_content_len, out_content)

    # Send UDP message
    print "Sending data to {}, port {}".format(address, port)
    s.send(data)

    # Receive reply
    while True:
        print "Receiving data on UDP socket.."
        recv_data_remaining = 1
        content = ""
        
        while recv_data_remaining != 0:
            data_recv = s.recv(BUFFER_SIZE)
            recv_cid, recv_ack, recv_eom, recv_data_remaining, recv_content_len, recv_content = struct.unpack(PACKET_FORMAT, data_recv)
            
            # Decrypt if not end of message.
            if recv_eom == False:
                recv_content = encrypt_decrypt_message(recv_content, recv_enc_keys[recv_enc_key_index], recv_content_len)
                recv_enc_key_index = recv_enc_key_index + 1
            
            # Append received message part to already received parts.
            content = content + recv_content

        print "Full received message: '{}'\n".format(content)

        if recv_eom == True:
            print "Last message received."
            s.close()
            print "UDP socket closed.\n"
            break

        message_reversed = reverse_words_in_string(content)
        print "Replying on UDP socket with '{}'..\n".format(message_reversed)
        remain = len(message_reversed)
        for piece in pieces(message_reversed):
            remain -= len(piece)
            piece_enc = encrypt_decrypt_message(piece, sent_enc_keys[sent_enc_key_index], len(piece))
            sent_enc_key_index = sent_enc_key_index + 1
            if remain == 0:
                out_eom = True
            data = struct.pack(PACKET_FORMAT, client_id, out_ack, out_eom, remain, len(piece), piece_enc)
            s.send(data)
            out_eom = False
    return

def main():
    USAGE = 'usage: %s<server address> <server port> <message>' % sys.argv[0]
    try:
        server_address = str(sys.argv[1])
        server_tcpport = int(sys.argv[2])
        message = str(sys.argv[3]) + " ENC MUL"
    except (IndexError, ValueError):
        sys.exit(USAGE)
        
    send_and_receive_tcp(server_address, server_tcpport, message)

if __name__=='__main__':
	main()
